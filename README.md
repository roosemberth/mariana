This project extracts a list of addresses from a PDF file from
<http://enjoylausanne.ch> and converts it to a gpx file that may be imported
as `favourites.gpx` in OSMAnd.

# Data

The PDF file was downloaded from <https://enjoylausanne.ch/commerces/pdf> on
Sat 20 Feb 2021 11:36:17 AM CET.

# Requirements

This project requires the following commands: curl, jq, make, pdftoxml, sh,
xmllint, xsltproc.

# Process description

In order to generate the GPX file, you just need to run `make`.
If you're interested in the details on how the files are processed, you may
continue reading this section.

The Makefile will generate an XML (`catalog.xml`) from the PDF.
The XML has the following format:

```xml
<?xml version="1.0"?>
<catalog xmlns:exsl="http://exslt.org/common">
  <place category="Alimentaire / Boissons">
    <name>Aux Sp&#xE9;cialit&#xE9;s de la Palud</name>
    <addr>Place de la Palud 9, 1003 Lausanne</addr>
    <phone>+41 21 312 64 51</phone>
  </place>
  ...
</catalog>
```

This xml is then used to generate `addresses.txt` which contains a list of
postal addresses.

The addresses are then resolved into coordinates using the [OSM nominatim tool].
Then are converted back to an XML representation (`addresses.xml`) with the
following format:

```xml
<addresses>
  <entry>
    <addr>Av. d'Ouchy 76, 1006 Lausanne</addr>
    <lat>46.5073297</lat>
    <lon>6.6271873</lon>
  </entry>
  ...
</addresses>
```

As of writing there are about 300 addresses so querying them may take a couple of
minutes (5 mins last time I checked).

Some addresses may fail to be resolved due to temporary errors in the nominatim
tool or because it can't find the specified addresses.
If you see any error messages such as 'Failed to solve coordinates for...', you
may extract a list of the missing addresses with the following command:

```sh
$ xmllint addresses.xml --xpath '//addr/text()' | grep -vf - addresses.txt > failed.txt
```

You can try to manually clean / fix these addresses.
You can retry to retrieve them using the following command:

```sh
$ cat failed.txt | ./fetchAddresses.sh | xmllint --format /dev/stdin
```

Merging failed resolutions is not supported, so this command is merely a preview
and you MUST modify any addresses in the `catalog.xml` file.
Please open an issue if you think this deserves to be looked into.
Don't forget to run make again to refresh the `addresses.txt` file!

Mind that **any missing addresses** from the `addresses.xml` file **will be
silently discarded.**

[OSM nominatim tool]: https://wiki.openstreetmap.org/wiki/Nominatim

# License

GPLv3
