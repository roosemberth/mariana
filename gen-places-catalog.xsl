<?xml version="1.0" encoding="UTF-8"?>
<!--
  (C) Roosembert Palacios, 2021

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

<xsl:transform version="1.0"
               xmlns:exsl="http://exslt.org/common"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <xsl:variable name="lexed">
    <xsl:call-template name="ingestPDF"/>
  </xsl:variable>
  <xsl:variable name="parsed">
    <xsl:apply-templates select="exsl:node-set($lexed)/placesArray"/>
  </xsl:variable>
  <xsl:copy-of select="$parsed"/>
</xsl:template>

<xsl:template name="ingestPDF">
  <placesArray>
    <xsl:for-each select="//PAGE/TEXT">
      <xsl:choose>
        <!-- Ignore headers -->
        <xsl:when test="*/text()='Nom'"/>
        <xsl:when test="*/text()='Adresse'"/>
        <xsl:when test="*/text()='Téléphone'"/>
        <xsl:when test="*/@bold='yes'">
          <type><xsl:call-template name="concatTokens"/></type>
        </xsl:when>
        <xsl:when test="@x &lt; 250">
          <name><xsl:call-template name="concatTokens"/></name>
        </xsl:when>
        <xsl:when test="@x &lt; 400">
          <addr><xsl:call-template name="concatTokens"/></addr>
        </xsl:when>
        <xsl:otherwise>
          <phone><xsl:call-template name="concatTokens"/></phone>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </placesArray>
</xsl:template>

<xsl:template name="concatTokens">
  <xsl:for-each select="TOKEN">
    <xsl:value-of select="."/>
    <xsl:if test='position()!=last()'>
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="placesArray">
  <catalog>
    <xsl:variable name="extractedPlaces">
      <xsl:apply-templates select="name"/>
    </xsl:variable>
    <xsl:variable name="places">
      <xsl:apply-templates select="exsl:node-set($extractedPlaces)" mode="collapseNameAndAddr"/>
    </xsl:variable>
    <xsl:copy-of select="$places"/>
  </catalog>
</xsl:template>

<!-- Calculates the index of the attribute before the next 'name' attribute. -->
<xsl:template name="findPlaceEndIdx">
  <xsl:param name="node" select="."/>
  <xsl:choose>
    <xsl:when test="$node[name()='name']">
      <xsl:call-template name="findPlaceEndIdx">
        <xsl:with-param name="node" select="$node/following-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:choose>
        <xsl:when test="$node/following-sibling::name">
          <xsl:value-of select="count($node/following-sibling::name[1]/preceding-sibling::*) + 1" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="count($node/../*) + 1" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="placesArray/name">
  <!-- Guard not to process remainders of names broken across multiple nodes. -->
  <xsl:if test="preceding-sibling::*[1][name()!='name']">
    <xsl:variable name="rgStart" select="count(preceding-sibling::*)" />
    <xsl:variable name="rgStop">
      <xsl:call-template name="findPlaceEndIdx"/>
    </xsl:variable>
    <place>
      <xsl:attribute name="category">
        <xsl:value-of select="preceding-sibling::type[1]" />
      </xsl:attribute>
      <xsl:for-each select="../*[position() &gt; $rgStart and position() &lt; $rgStop]">
        <xsl:if test="name()!='type'">
          <xsl:copy-of select="." />
        </xsl:if>
      </xsl:for-each>
    </place>
  </xsl:if>
</xsl:template>

<xsl:template match="place" mode="collapseNameAndAddr">
  <xsl:copy>
    <xsl:copy-of select="@*"/>
    <name>
      <xsl:for-each select="name">
        <xsl:if test="preceding-sibling::name">
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:copy-of select="text()" />
      </xsl:for-each>
    </name>
    <addr>
      <xsl:for-each select="addr">
        <xsl:if test="preceding-sibling::addr">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:copy-of select="text()" />
      </xsl:for-each>
    </addr>
    <xsl:copy-of select="phone" />
  </xsl:copy>
</xsl:template>

</xsl:transform>
