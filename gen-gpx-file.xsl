<?xml version="1.0" encoding="UTF-8"?>
<!--
  (C) Roosembert Palacios, 2021

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
-->

<xsl:transform version="1.0"
               xmlns:exsl="http://exslt.org/common"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="catalog"/>
<xsl:param name="places" select="document($catalog)/catalog"/>

<xsl:template match="/">
  <xsl:variable name="merged">
    <xsl:apply-templates select="/addresses/entry"/>
  </xsl:variable>
  <gpx version="1.1" creator="mariana-0.0.0"
       xmlns="http://www.topografix.com/GPX/1/1"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    <xsl:apply-templates select="exsl:node-set($merged)" mode="renderGPX"/>
  </gpx>
</xsl:template>

<xsl:template match="/addresses/entry">
  <xsl:apply-templates select="$places/place[addr=current()/addr]" mode="mergeCoords">
    <xsl:with-param name="address" select="current()"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="place" mode="mergeCoords">
  <xsl:param name="address"/>
  <xsl:copy>
    <xsl:copy-of select="@*"/>
    <xsl:copy-of select="*"/>
    <xsl:copy-of select="$address/lat"/>
    <xsl:copy-of select="$address/lon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="place" mode="renderGPX">
  <wpt>
    <xsl:attribute name="lat">
      <xsl:value-of select="lat"/>
    </xsl:attribute>
    <xsl:attribute name="lon">
      <xsl:value-of select="lon"/>
    </xsl:attribute>
    <xsl:copy-of select="name"/>
    <type>
      <xsl:value-of select="@category"/>
    </type>
    <extensions>
      <color>
        <xsl:value-of select="'#b4003133'"/>
      </color>
    </extensions>
  </wpt>
</xsl:template>

</xsl:transform>
