all: favourites.gpx catalog.xml addresses.xml

favourites.gpx: addresses.xml catalog.xml gen-gpx-file.xsl
	xsltproc --stringparam catalog catalog.xml gen-gpx-file.xsl $< \
		| xmllint --format /dev/stdin > $@

addresses.xml: addresses.txt fetchAddresses.sh
	./fetchAddresses.sh < $< > $@

addresses.txt: catalog.xml
	xmllint $< --xpath '//place/addr/text()' | sort -u > $@

catalog.xml: Commerces.xml gen-places-catalog.xsl
	xsltproc gen-places-catalog.xsl $< | xmllint --format /dev/stdin > $@

Commerces.xml: Commerces.pdf
	pdftoxml $< $@

clean:
	rm -f addresses.txt addresses*.xml
	rm -f catalog.xml Commerces.xml
