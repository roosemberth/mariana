#!/usr/bin/env sh

# (C) Roosembert Palacios, 2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

# Exit codes
E_SUCCESS=0
E_USER=1

die() {
    retval="$1"; shift
    if [ $# -eq 0 ]; then
        cat <&0 >&2
    else
        printf "%s" "$@" >&2; echo >&2
    fi
    if [ "$retval" = $E_USER ]; then
        echo "Run with --help for more information." >&2
    fi
    exit "$retval"
}

BASENAME="$0"

usage() {
  cat <<-EOF
	$BASENAME: Resolve addresses from stdin into an XML file of coordinates.

	Usage:
	$BASENAME [-h]
	$BASENAME [-s] < addresses.txt > addresses.xml

	Description:
	  This program resolves postal addresses to GPS coordinates using the OSM
	  Nominatim tool. The resulting XML file has the following format:

	  <addresses>
	    <entry>
	      <addr>Avenue de Béthusy 86, 1012 Lausanne</addr>
	      <lat>46.5261607</lat>
	      <lon>6.6509766</lon>
	    </entry>
	  </addresses>

	  Note that the produced XML will not be pretty-printed.
	  A warning will be generated if no addresses were provided.

	Options
	  -h, --help
	      Show this help message and exit.
	  --no-progress
	      Do not display progress while querying addresses.
	EOF
}

NO_PROGRESS=""

while [ $# -gt 0 ]; do
    opt="$1"; shift
    case "$opt" in
        (-h|--help) usage; exit ;;
        (--no-progress) NO_PROGRESS=1 ;;
        (-*) die $E_USER 'Unknown option: %s' "$opt" ;;
        (*) die $E_USER 'Trailing argument: %s' "$opt" ;;
    esac
done

command -v curl > /dev/null || die $E_USER 'Missing required command curl'
command -v jq > /dev/null || die $E_USER 'Missing required command jq'

COUNT=0

process_address() {
  if [ -z "$NO_PROGRESS" ]; then
    printf "\rResolving coordinates for address #$COUNT... " >&2
  fi
  # Breakdown since jq doesn't fail if data format is not as expected.
  ( DATA="$(curl -sL "http://nominatim.openstreetmap.org/search?format=json&q=$1")"
    [ $? = 0 ] || exit $?
    LAT="$(echo "$DATA" | jq -er '.[0].lat' 2>/dev/null)"
    [ $? = 0 ] || exit $?
    LON="$(echo "$DATA" | jq -er '.[0].lon' 2>/dev/null)"
    [ $? = 0 ] || exit $?
    printf '\n<entry><addr>%s</addr><lat>%s</lat><lon>%s</lon></entry>' \
      "$1" "$LAT" "$LON"
  ) || (echo "Failed to solve coordinates for '$1'" >&2)
}

printf '<addresses>'
while read -t1 addr; do
  if [ ! -z "$addr" ]; then
    COUNT=$(($COUNT+1))
    process_address "$addr"
  fi
done
printf '\n</addresses>'

# Required since the progress indicator does not print a trailing line.
echo >&2

if [ "$COUNT" = 0 ]; then
  echo "WARNING: No addresses were specified. Run with --help for info." >&2
fi
